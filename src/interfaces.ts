export type ValidationResult = Promise<ValidationError[]|null>|ValidationError[]|null;

export interface ValidationError
{
    errorKey: string;
    path?: (string|number)[];
    details?: { [key: string]: any };
    causes?: ValidationError[];
}

export interface NoEarlyExitProps
{
    noEarlyExit?: boolean;
    
    [key: string]: any;
}

export type ValidationFunction<TProps = {}> = (value: any, props: TProps) => ValidationResult;