import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isUndefined<TProps>(): ValidationFunction<TProps>
{
    return value => undefined === value ? null : [{
        errorKey: BAD_TYPE,
        details: {
            expected: 'undefined',
            got: typeof value,
            value
        }
    }];
}