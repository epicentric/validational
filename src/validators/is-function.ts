import {ValidationFunction} from '../interfaces';
import {isTypeOf} from './is-type-of';

export function isFunction<TProps>(): ValidationFunction<TProps>
{
    return isTypeOf('function');
}