import {NoEarlyExitProps, ValidationError, ValidationFunction} from '../interfaces';
import {isIterable} from './is-iterable';
import {hasProperties} from './has-properties';
import {isFunction} from './is-function';
import {anyOf} from './any-of';

export function allValues<TProps extends NoEarlyExitProps>(validator: ValidationFunction<TProps>): ValidationFunction<TProps>
{
    const preconditionValidator = anyOf([
        isIterable(),
        hasProperties({
            values: isFunction()
        })
    ]);

    return async (value, props) => {
        const preconditionErrors = await preconditionValidator(value, props);
        if (preconditionErrors)
            return preconditionErrors;

        const noEarlyExit = props.noEarlyExit;

        const errors: ValidationError[] = [];
        
        let index = 0;
        for (const item of (value.values ? value.values() : value))
        {
            const currentErrors = await validator(item, props);

            if (currentErrors)
            {
                for (const error of currentErrors)
                {
                    (error.path || (error.path = [])).unshift(index);
                }

                errors.push(...currentErrors);

                if (!noEarlyExit)
                    break;
            }
            
            index++;
        }
        
        return errors;
    };
}