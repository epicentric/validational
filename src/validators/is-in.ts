import {ValidationFunction} from '../interfaces';
import {OUT_OF_RANGE} from '../error-keys';

export function isIn<TProps>(values: any[]): ValidationFunction<TProps>
{
    return value => values.includes(value) ? null : [{
        errorKey: OUT_OF_RANGE,
        details: {
            expected: values,
            value
        }
    }];
}