import {ValidationFunction} from '../interfaces';
import {OUT_OF_RANGE} from '../error-keys';

export function isGreaterThanOrEqualTo<TProps, T>(min: T): ValidationFunction<TProps>
{
    return value => min <= value ? null : [{
        errorKey: OUT_OF_RANGE,
        expected: { min },
        value
    }];
}