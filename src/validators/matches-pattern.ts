import {ValidationFunction} from '../interfaces';
import {isString} from './is-string';
import {PATTERN_MISMATCH} from '../error-keys';

export function matchesPattern<TProps>(pattern: RegExp): ValidationFunction<TProps>
{
    const preconditionValidator = isString();
    
    return (value, props) => {
        const preconditionErrors = preconditionValidator(value, props);
        if (preconditionErrors)
            return preconditionErrors;
        
        return pattern.test(value) ? null : [{
            errorKey: PATTERN_MISMATCH,
            details: {
                pattern,
                value
            }
        }];
    };
}