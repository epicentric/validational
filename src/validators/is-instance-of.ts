import {ValidationFunction} from '../interfaces';
import {BAD_INSTANCE_TYPE} from '../error-keys';

type Constructable<T = {}> = new (...args: any[]) => T;

export function isInstanceOf<TProps>(type: Constructable<unknown>): ValidationFunction<TProps>
{
    return value => value instanceof type ? null : [{
        errorKey: BAD_INSTANCE_TYPE,
        details: {
            expected: type,
            value
        }
    }];
}