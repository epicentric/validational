import {NoEarlyExitProps, ValidationError, ValidationFunction} from '../interfaces';

export function allOf<TProps extends NoEarlyExitProps>(validators: ValidationFunction<TProps>[]): ValidationFunction<TProps>
{
    return async (value, props) => {
        const errors: ValidationError[] = [];

        for (const validator of validators)
        {
            const error = await validator(value, props);

            if (error)
            {
                errors.push(...error);

                if (!props.noEarlyExit)
                    break;
            }
        }

        return errors.length ? errors : null;
    };
}