import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isInteger<TProps>(): ValidationFunction<TProps>
{
    return value => Number.isSafeInteger(value) ? null : [{
        errorKey: BAD_TYPE,
        details: {
            expected: 'integer',
            value
        }
    }];
}