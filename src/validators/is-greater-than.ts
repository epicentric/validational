import {ValidationFunction} from '../interfaces';
import {OUT_OF_RANGE} from '../error-keys';

export function isGreaterThan<TProps, T>(greaterThan: T): ValidationFunction<TProps>
{
    return value => greaterThan < value ? null : [{
        errorKey: OUT_OF_RANGE,
        expected: { greaterThan },
        value
    }];
}